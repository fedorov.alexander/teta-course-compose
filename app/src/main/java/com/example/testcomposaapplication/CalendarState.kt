package com.example.testcomposaapplication

sealed class ScreenState {
    data class CalendarState(
        val year: String,
        val monthText: String,
        val daysCount: Int,
        val monthCounter: Int,
        val calendarList: List<Day?>,
        val selectedDay: Int?,
    ) : ScreenState()

    data class DetailsState(
        val selectedDay: Int,
    ) : ScreenState()
}


data class Day(
    val text: String,
    val holiday: Boolean,
    val selected: Boolean,
    val numberOfDay: Int,
)
