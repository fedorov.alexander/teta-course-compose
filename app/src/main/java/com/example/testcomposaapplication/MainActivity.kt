package com.example.testcomposaapplication

import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.ButtonDefaults.buttonColors
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.testcomposaapplication.ScreenState.CalendarState
import com.example.testcomposaapplication.ScreenState.DetailsState
import com.example.testcomposaapplication.ui.theme.DesignSystemTheme
import com.example.testcomposaapplication.ui.theme.palette.ControlPrimaryActive
import com.example.testcomposaapplication.ui.theme.palette.TextPrimary
import com.example.testcomposaapplication.ui.theme.palette.TextSecondary
import com.example.testcomposaapplication.ui.theme.palette.TextTertiary
import java.util.*

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DesignSystemTheme {
                Surface(color = DesignSystemTheme.colors.backgroundPrimary) {
                    val context = LocalContext.current
                    var screenState by remember { mutableStateOf(calendarState(context = context) as ScreenState) }
                    when (val state = screenState) {
                        is CalendarState -> CalendarScreen(state) {
                            screenState = it
                        }
                        is DetailsState -> {
                            DetailsScreen(state) {
                                screenState = it
                            }
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun CalendarScreen(
    calendarState: CalendarState,
    updateState: (ScreenState) -> Unit,
) {
    val context = LocalContext.current

    Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
        Box(modifier = Modifier
            .fillMaxWidth()
            .padding(top = 12.dp)) {
            Row(Modifier.padding(start = 20.dp, top = 20.dp),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically) {
                Text(text = calendarState.monthText + " " + calendarState.year,
                    style = DesignSystemTheme.typography.h3.medium)
                Icon(imageVector = ImageVector.vectorResource(id = R.drawable.ic_button_up_down),
                    contentDescription = null,
                    tint = Color.Unspecified,
                    modifier = Modifier
                        .padding(start = 8.dp)
                        .size(16.dp)
                        .clickable(interactionSource = remember { MutableInteractionSource() },
                            indication = rememberRipple(bounded = false),
                            onClick = {/*TODO*/ }))
                Spacer(modifier = Modifier.weight(1f))
                IconButton(onClick = {
                    updateState(calendarState(countMonth = calendarState.monthCounter - 1,
                        context = context))
                }) {
                    Icon(
                        imageVector = ImageVector.vectorResource(id = R.drawable.ic_button_left),
                        contentDescription = null,
                        tint = Color.Unspecified,
                    )
                }
                IconButton(onClick = {
                    updateState(calendarState(calendarState.monthCounter + 1, context = context))
                }) {
                    Icon(
                        imageVector = ImageVector.vectorResource(id = R.drawable.ic_button_right),
                        contentDescription = null,
                        tint = Color.Unspecified,
                    )
                }
            }
        }
        LazyVerticalGrid(cells = GridCells.Fixed(DAYS_IN_WEEK),
            modifier = Modifier.defaultMinSize(minHeight = 260.dp)) {
            items(calendarState.calendarList.size + DAYS_IN_WEEK) { item ->
                if (item < DAYS_IN_WEEK) {
                    Text(
                        modifier = Modifier.padding(bottom = 14.dp),
                        text = stringArrayResource(id = R.array.days).getOrNull(item)?.uppercase()
                            ?: EMPTY_STRING,
                        textAlign = TextAlign.Center,
                        style = DesignSystemTheme.typography.p3.mediumUppercase,
                        color = TextSecondary,
                    )
                } else {
                    calendarState.calendarList.getOrNull(item - DAYS_IN_WEEK)?.let { day ->
                        return@items Text(text = day.text,
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .padding(bottom = 16.dp)
                                .clickable {
                                    updateState(calendarState(
                                        countMonth = calendarState.monthCounter,
                                        selected = if (!day.selected) day.numberOfDay else null,
                                        context = context,
                                    ))
                                },
                            style = DesignSystemTheme.typography.h3.regular,
                            color = when {
                                day.selected -> ControlPrimaryActive
                                day.holiday -> TextTertiary
                                else -> TextPrimary
                            })
                    } ?: Spacer(Modifier.wrapContentSize())
                }
            }
        }
        Spacer(modifier = Modifier.height(26.dp))
        Button(onClick = {
            calendarState.selectedDay ?: return@Button
            updateState(DetailsState(calendarState.selectedDay))
        },
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = 42.dp,
                    end = 36.dp,
                ),
            enabled = calendarState.selectedDay != null,
            shape = RoundedCornerShape(6.dp),
            colors = buttonColors(
                backgroundColor = DesignSystemTheme.colors.controlsPrimaryActive,
                contentColor = Color.White,
                disabledBackgroundColor = DesignSystemTheme.colors.controlsInactive,
                disabledContentColor = Color.White,
            )) {
            Text(text = stringResource(R.string.open_details),
                style = DesignSystemTheme.typography.p2.medium)
        }
    }
}

fun calendarState(countMonth: Int = 0, selected: Int? = null, context: Context): CalendarState =
    Calendar.getInstance().let { calendar ->
        calendar.add(Calendar.MONTH, countMonth)
        calendar.set(Calendar.DAY_OF_MONTH, 1)

        CalendarState(
            year = calendar[Calendar.YEAR].toString(),
            monthText = context.resources.getStringArray(R.array.months)[calendar.get(Calendar.MONTH)],//stringArrayResource[calendar.get(Calendar.MONTH)],
            daysCount = calendar.getActualMaximum(Calendar.DATE),
            monthCounter = countMonth,
            calendarList = monthArray(
                maxSize = countDaysBeforeFirst(calendar.get(Calendar.DAY_OF_WEEK)) + calendar.getActualMaximum(
                    Calendar.DATE),
                start = countDaysBeforeFirst(calendar.get(Calendar.DAY_OF_WEEK)),
                selectedDay = selected,
            ),
            selectedDay = selected,
        )
    }

private fun countDaysBeforeFirst(dayOfWeek: Int) = when (dayOfWeek) {
    1 -> 6
    else -> dayOfWeek - 2
}

private fun monthArray(maxSize: Int, start: Int, selectedDay: Int?) = List(maxSize) { index ->
    if (index < start) {
        null
    } else {
        val numberOfDay = (index + 1 - start)
        Day(
            text = numberOfDay.toString(),
            numberOfDay = numberOfDay,
            holiday = isDayHoliday(index),
            selected = numberOfDay == selectedDay,
        )
    }
}

private fun isDayHoliday(index: Int) = (index % DAYS_IN_WEEK).let {
    it == 5 || it == 6
}

@Composable
fun DetailsScreen(
    state: DetailsState,
    function: (ScreenState) -> Unit,
) {
    val context = LocalContext.current
    BackHandler(enabled = true) {
        function(calendarState(selected = state.selectedDay, context = context))
    }
    Column(modifier = Modifier
        .fillMaxSize()
        .padding(
            start = 21.dp,
            top = 24.dp,
            end = 21.dp,
        )) {
        Text(text = stringResource(R.string.details_title, state.selectedDay),
            modifier = Modifier
                .wrapContentSize(align = Alignment.CenterStart)
                .padding(bottom = 13.dp),
            style = DesignSystemTheme.typography.h3.medium)
        val detailsArray = stringArrayResource(id = R.array.details)
        LazyColumn {
            items(detailsArray.size) { index ->
                Row(modifier = Modifier.padding(
                    start = 28.dp,
                    end = 23.dp,
                    bottom = 16.dp,
                )) {
                    val textStyle = DesignSystemTheme.typography.h3.regular
                    Icon(imageVector = ImageVector.vectorResource(id = R.drawable.ic_bullet),
                        contentDescription = null,
                        tint = Color.Unspecified,
                        modifier = Modifier.height(textStyle.lineHeight.value.dp))
                    Text(
                        text = detailsArray.getOrNull(index) ?: EMPTY_STRING,
                        modifier = Modifier.padding(
                            start = 12.dp,
                        ),
                        style = textStyle,
                    )
                }
            }
        }
    }
}

private const val DAYS_IN_WEEK = 7
private const val EMPTY_STRING = ""
